package com.example.mr_oficial.ui;

import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mr_oficial.R;

public class ejemplo_oferta extends Fragment {


    private EjemploOfertaViewModel ejemploOfertaViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        ejemploOfertaViewModel =
                ViewModelProviders.of(this).get(EjemploOfertaViewModel.class);
        View root = inflater.inflate(R.layout.ejemplo_oferta_fragment, container, false);
        final TextView textView = root.findViewById(R.id.text_oferta);
        ejemploOfertaViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }

}
