package com.example.mr_oficial.ui;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class EjemploOfertaViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private MutableLiveData<String> mText;

    public EjemploOfertaViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("OFERTAS DISPONIBLES");
    }

    public LiveData<String> getText() {
        return mText;
    }
}
