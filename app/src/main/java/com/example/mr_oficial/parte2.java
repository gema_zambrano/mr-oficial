package com.example.mr_oficial;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class parte2 extends Activity {
    Button button5;
    ImageView imageninicio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parte2);
        button5=findViewById(R.id.button5);
        imageninicio=findViewById(R.id.imageninicio);
        imageninicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2=new Intent(parte2.this,menuUsuario.class);
                startActivity(intent2);
            }
        });
        button5.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent2=new Intent(parte2.this,parte3.class);
                startActivity(intent2);
            }
        });
    }
}
